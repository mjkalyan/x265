(in-package :cl-user)
(defpackage x265
  (:use :cl
        :cffi))
(in-package :x265)

(define-foreign-library libx265
  (:unix (:or "libx265.so" "libx265.so.199"))
  (t (:default "libx265")))

(use-foreign-library libx265)

;; TODO define "special pointer types" for the return types (with defctype like below?)
;; (defctype x265-param :pointer)

(defcfun "x265_encoder_open_199" :pointer ; why is *_199 exposed but not with _199?
  (x265-param :pointer))
(defcfun "x265_encoder_open" :pointer
  (x265-param :pointer))

(defcfun "x265_param_alloc" :pointer)

(defcfun "x265_param_default_preset" :int
  (x265-param :pointer)
  (preset :char)
  (tune :char))

(defcfun "x265_param_apply_profile" :int
  (x265-param :pointer)
  (profile :char))

(defcfun "x265_param_parse" :int
  (p :pointer)
  (name :char)
  (value :char))

(defcfun "x265_param_free" :void
  (x265-param :pointer))

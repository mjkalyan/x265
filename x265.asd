(defsystem "x265"
  :author "James Kalyan <contrib_x@protonmail.com>"
  :maintainer "James Kalyan <contrib_x@protonmail.com>"
  :license "GPL-3+"
  :homepage "https://gitlab.com/mjkalyan/x265"
  :version "0.1.0"
  :depends-on ("cffi")
  :components ((:module "src"
                :components
                ((:file "x265"))))
  :description "An x265 FFI in Common Lisp"
  :long-description
  #.(uiop:read-file-string
     (uiop:subpathname *load-pathname* "README.org"))
  :in-order-to ((test-op (test-op "x265-test"))))
